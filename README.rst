#######
OpenREM
#######
====================

.. image:: https://coveralls.io/repos/bitbucket/openrem/openrem/badge.svg?branch=develop
    :target: https://coveralls.io/bitbucket/openrem/openrem?branch=develop

.. image:: https://img.shields.io/pypi/v/openrem.svg?
    :target: https://badge.fury.io/py/openrem

.. image:: https://img.shields.io/pypi/pyversions/openrem.svg?
    :target: https://badge.fury.io/py/openrem

.. image:: https://img.shields.io/bitbucket/issues/openrem/openrem.svg?
    :target: https://bitbucket.org/openrem/openrem/issues?status=new&status=open

.. image:: https://www.quantifiedcode.com/api/v1/project/ed6462bb44f4462189fd8777ab26e8dd/snapshot/origin:develop:HEAD/badge.svg?
    :target: https://www.quantifiedcode.com/app/project/ed6462bb44f4462189fd8777ab26e8dd
    :alt: QuantifiedCode issues

.. image:: https://api.codacy.com/project/badge/Grade/2117af7281134e42ace3efe5fc4a5255
    :target: https://www.codacy.com/app/OpenREM/openrem?utm_source=openrem@bitbucket.org&amp;utm_medium=referral&amp;utm_content=openrem/openrem&amp;utm_campaign=Badge_Grade

.. image:: https://api.codacy.com/project/badge/Coverage/2117af7281134e42ace3efe5fc4a5255
    :target: https://www.codacy.com/app/OpenREM/openrem?utm_source=openrem@bitbucket.org&amp;utm_medium=referral&amp;utm_content=openrem/openrem&amp;utm_campaign=Badge_Coverage

OpenREM is a Django app to extract, store and export Radiation Exposure
Monitoring related information, primarily from DICOM files.

This is release 0.7.4 containing a few essential fixes. Please review the
`release notes <http://docs.openrem.org/en/0.7.4/release-0.7.4.html>`_ for details.

Full documentation can be found on Read the Docs: http://docs.openrem.org/, and a demo site can be seen at
http://demo.openrem.org; username `demo` password `demo`

For upgrades, please review the `version release notes <http://docs.openrem.org/en/0.7.4/release-0.7.4.html>`_. For
fresh installs, please review the `install docs <http://docs.openrem.org/en/0.7.4/install.html>`_.

Contribution of code, ideas, bug reports documentation is all welcome.
Please feel free to fork the repository and send me pull requests. See
`the website <http://openrem.org/getinvolved>`_ for more information.

There is a developer demo site, which at times has a working demo of recent code, but might not, and 
might be broken. It can be found at http://dev.openrem.org; username `demo` password `demo`

OpenREM currently ships with charts that use the HighCharts libraries. This is free to use in a non-commercial setting.
For other environments please contact Highcharts for licencing. For more information, please see
`their website <http://highcharts.com>`_, or the
`Non-Commercial FAQ <https://shop.highsoft.com/faq#Non-Commercial-0>`_.