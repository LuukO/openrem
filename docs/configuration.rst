#############
Configuration
#############

See also: ``local_settings.py`` :ref:`localsettingsconfig`

.. toctree::
    :maxdepth: 2

    i_deletesettings
    i_displaynames
    i_not_patient_indicator